import os


def list_all_static_css(app_static_dir=None, app_file=None):
    """Auto-generates a list of all static css files.

    These are files that need to be included in the html template.

    Parameters
    ----------
    app_static_dir : str (filepath) or None
        Used to load an inheriting app's css. This is the filepath relative
        to the app where the static files are located. Get this from the
        app.angular.models script with `current_app._static_folder` where
        `current_app` is imported from `flask`. Ignored if `app_file` is None.
    app_file : filepath
        Used to load an inheriting app's css. This is the absolute filepath
        to the `app.angular.models` script. Just pass `__file__` as this
        parameter from that script. Ignored if `app_static_dir` is None.

    Returns
    -------
    css_lst : dict (str -> list of str)
        `css_list['core']` is a list of all core css files relative to the
        flaskular core css directory. `css_list['app']` exists if the
        parameters are not None, and is a list of all app css files
        relative to the app static directory.
    """
    static_dir = get_abs_static_dir('static', __file__)
    css_lst = {}
    css_lst['core'] = list_css_in_dir(static_dir)

    if app_static_dir is not None and app_file is not None:
        app_dir = get_abs_static_dir(app_static_dir, app_file)
        css_lst['app'] = list_css_in_dir(app_dir)

    return css_lst


def list_css_in_dir(dir):
    """Lists all css files in a directory or its children.

    Internal to `list_all_static_css()`

    Parameters
    ----------
    dir : str (filepath)
        The directory to search for css files.

    Returns
    -------
    css_lst : lst of string
        A list of all filenames of css files, relative to dir.
    """

    def _include_css(root, dirs, file):
        return file.endswith('.css')
    css_list = _walk(dir, _include_css)

    return css_list


def list_all_static_js(app_static_dir=None, app_file=None):
    """Lists all js scripts in the flaskular directory.

    Modules first, and then the rest.

    Parameters
    ----------
    app_static_dir : str (filepath) or None
        Used to load an inheriting app's css. This is the filepath relative
        to the app where the static files are located. Get this from the
        app.angular.models script with `current_app._static_folder` where
        `current_app` is imported from `flask`. Ignored if `app_file` is None.
    app_file : filepath
        Used to load an inheriting app's css. This is the absolute filepath
        to the `app.angular.models` script. Just pass `__file__` as this
        parameter from that script. Ignored if `app_static_dir` is None.

    Returns
    -------
    js_lst : dict (str -> list of str)
        `js_list['core']` is a list of all core css files relative to the
        flaskular core css directory. `js_list['app']` exists if the
        parameters are not None, and is a list of all app css files
        relative to the app static directory. The lists exclude all
        test *.spec.js files.
    """
    static_dir = get_abs_static_dir('static', __file__)

    modules = list_js_modules_in_dir(static_dir)
    nonmodules = list_js_nonmodules_in_dir(static_dir)

    js_list = {}
    js_list['core'] = modules + nonmodules

    if app_static_dir is not None and app_file is not None:
        app_dir = get_abs_static_dir(app_static_dir, app_file)
        app_modules = list_js_modules_in_dir(app_dir)
        app_nonmodules = list_js_nonmodules_in_dir(app_dir)
        js_list['app'] = app_modules + app_nonmodules

    return js_list


def list_js_modules_in_dir(dir):
    """Lists all js files that are modules in the given directory.

    Parameters
    ----------
    dir : str (filepath)
        The directory to search for the js files.

    Returns
    -------
    js_list : lst of string
        A list of all filenames of js modules, relative to the given directory
        but excluding the test *.spec.js.
    """

    def _include_only_modules(root, dirs, file):
        return file.endswith('.js') and 'module' in file \
            and 'spec' not in file
    return _walk(dir, _include_only_modules)


def list_js_nonmodules_in_dir(dir):
    """Lists all js files that are not modules in the given directory.

    Parameters
    ----------
    dir : str (filepath)
        The directory to search for the js files.

    Returns
    -------
    js_list : lst of string
        A list of all filenames of js scripts that are not modules, relative to
        the given directory but excluding the test *.spec.js.
    """
    def _dont_include_modules(root, dirs, file):
        return file.endswith('.js') and 'module' not in file \
            and 'spec' not in file
    return _walk(dir, _dont_include_modules)


def _walk(dir, should_include):
    """
    Walks a directory, adding files to a list according to the function filter.

    Parameters
    ----------
    dir : str (filepath)
        The directory to walk
    should_include : fcn (root, dirs, file -> bool)
        Determines whether or not each file walked should belong in the list.
        @param root {str (filepath)} The root directory of the file being
                                     examined.
        @param dirs {str (filepath)} The directories inside of root of the
                                     file being examined.
        @param file {str} The file name of the file being examined.
        @returns {bool} True if the file should be included, false otherwise.

    Returns
    -------
    lst : list of str
        The filepaths of all files in dir (and its subdirectories) which met
        the conditions specified by should_include.
    """
    lst = []
    for root, dirs, files in os.walk(dir):
        for file in files:
            if should_include(root, dirs, file):
                path = os.path.join(root, file)
                path = path.replace(dir + '/', '')
                lst.append(path)
    return lst


def get_abs_static_dir(static_dir, calling_file):
    """Gets an absolute path to the static dir of the app calling this.

    Parameters
    ----------
    static_dir : str (filepath)
        The filepath of the current app.
    calling_file : __file__
        The `__file__` from the file calling this function. Assumes that this
        funciton is only called from flaskular/angular/models.py or
        myapp/angular/models.py (where myapp is a flaskular-inherited app).

    Returns
    -------
    abs_static_dir : str
        The absolute path to the static dir of the respective app.
    """
    abs_static_dir = os.path.abspath(os.path.join(
        os.path.dirname(calling_file),
        '..',
        static_dir
    ))
    return abs_static_dir

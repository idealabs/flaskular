from flaskular.test_base import TestController


class TestControllers(TestController):

    def test_angular(self):
        response = self.client.get('/')

        data = response.data.decode('utf-8')
        print(data)

        assert response.status_code == 200
        assert 'html' in data
        assert 'head' in data
        assert 'body' in data

        # make sure some scripts loaded
        assert 'flaskular.module.js' in data
        assert 'auth/auth.service.js' in data

        # make sure some css loaded
        assert 'flaskular.css' in data

    def test_get_core_static(self):
        response = self.client.get('/core/flaskular.module.js')
        assert response.status_code == 200

    def test_get_core_lib(self):
        response = self.client.get(
            '/corelib/angular-bootstrap/ui-bootstrap.js'
        )
        assert response.status_code == 200

"""
Defines the codes and messages which are used by the server to respond to the
client.
"""
from flaskular.codes import CodeManager


# Keys are the code id, values are the code message
_code_dict = {
    # Unknown
    'unknown': 'An unknown server error has occured in the auth module',

    # Register
    'register_success': 'Registration successful',
    'register_already_exists': 'Cannot register {user}; user already exits',

    # Login
    'login_success': 'Login successful',
    'login_unknown_user': 'Unknown user: {user}',
    'login_invalid_password': 'Invalid password',
    'login_dual': 'You have already logged in under account {other}',

    # Logout
    'logout_success': 'User {user} successfully logged out',
    'logout_nobody': 'Logout failed: nobody logged in',

    # Token Login
    'token_login_success': 'Token login successful',
    'token_login_expired': 'Login failed: token has expired',
    'token_login_invalid': 'Login failed: token is invalid',
    'token_login_logged_out': ('Login failed: user has logged out and the ' +
                               'token is no longer valid'),

    # Current user
    'current_user_success': 'Current user found: {user}',
    'current_user_fail': 'No user currently logged in',

    # User list
    'user_list_success': 'User list successfully fetched',

    # User dict
    'get_user_dict_success': ('Successfully fetched the user\'s data '
                              'dictionary.'),

    # Password Change
    'password_change_success': 'Password successfully changed',
    'password_change_incorrect_old': ('Cannot change password, old password '
                                      'is not correct.'),

    # Update profile
    'update_profile_success': 'Successfully updated the profile for {email}',
    'update_profile_not_authorized': 'Not authorized to update this profile',

    # Delete
    'delete_success': 'User successfully deleted',

    # Promote/Demote
    'promote_success': 'User {email} successfully promoted to admin',
    'demote_success': 'User {email} successfully demoted from admin'
}

# Auto-register codes
AUTH_CODES = CodeManager(_code_dict)

"""
Definitions of some useful exceptions for the authentication module.
"""


class AuthUserNotFound(Exception):
    """
    Thrown if an attempt is made to login with an unknown user.

    Attributes
    ----------
    user_id : str
        The id (email address) of the user which was not found in the database.

    Parameters
    ----------
    user_id : str
        Sets attribute `user_id`
    """

    def __init__(self, user_id):
        Exception.__init__(self)
        self.user_id = user_id


class AuthInvalidPassword(Exception):
    """
    Thrown if the given password is incorrect.
    """
    pass


class AuthLogoutNobody(Exception):
    """
    Thrown if a logout attempt is made with nobody logged in.
    """
    pass


class AuthTokenNoUser(Exception):
    """
    Thrown if no user can be found with the auth token.
    """
    pass


class AuthTokenInvalid(Exception):
    """
    Thrown if the password is invalid for the auth token.
    """
    pass


class AuthTokenLoggedOut(Exception):
    """
    Thrown if a token is used on a user who has actively logged out.
    """
    pass


class NotAuthorized(Exception):
    """
    Thrown if an unauthorized attempt is made to modify a secure part of
    the user's data.
    """
    pass

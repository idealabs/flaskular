import pytest
from itsdangerous import BadSignature

from flask.ext.security.utils import verify_password

from flaskular.test_base import TestModel
from .models import FlaskularUser, FlaskularRole
from . import exceptions as err


class TestAuthModels(TestModel):

    def test_admin_creation(self):
        """
        The app will create an admin on startup if no users already exists.
        Check to make sure it really did.
        """
        user = FlaskularUser.query.filter_by(email='admin@example.com').first()
        assert user is not None
        assert user.email == 'admin@example.com'
        assert verify_password('admin', user.password)

        admin_role = FlaskularRole.query.filter_by(name='admin').first()
        sub_role = FlaskularRole.query.filter_by(name='subscriber').first()

        assert user.has_role(admin_role)
        assert not user.has_role(sub_role)

    def test_role_creation(self):
        """
        The app will create two roles--admin and scubscriber--on startup if
        they do not yet exist. Check to make sure it really did.
        """
        roles = FlaskularRole.query.all()
        assert len(roles) == 2

        role_names = [role.name for role in roles]
        assert 'admin' in role_names
        assert 'subscriber' in role_names

    def test_role_create_new(self):
        role = FlaskularRole.create('test', 'A test role')

        assert role.name == 'test'
        assert role.description == 'A test role'

        found = FlaskularRole.query.filter_by(name='test').first()
        assert found is not None
        assert found == role

    def test_role_create_no_store(self):
        role = FlaskularRole.create('test2', 'Another test role', commit=False)

        assert role.name == 'test2'
        assert role.description == 'Another test role'

        found = FlaskularRole.query.filter_by(name='test2').first()
        assert found is None

    def test_role_representation(self):
        admin_role = FlaskularRole.query.filter_by(name='admin').first()
        assert admin_role.name == 'admin'
        assert str(admin_role) == '<Flaskular Role admin>'

    def test_user_representation(self):
        user = FlaskularUser.query.filter_by(email='admin@example.com').first()
        assert user.email == 'admin@example.com'
        assert str(user) == '<Flaskular User admin@example.com>'

    def test_create_user(self):
        FlaskularUser.create('test1@example.com', 'test1', 'subscriber')

        found = FlaskularUser.query.filter_by(
            email='test1@example.com'
        ).first()
        assert found is not None
        assert found.email == 'test1@example.com'
        assert verify_password('test1', found.password)

        admin_role = FlaskularRole.query.filter_by(name='admin').first()
        sub_role = FlaskularRole.query.filter_by(name='subscriber').first()

        assert not found.has_role(admin_role)
        assert found.has_role(sub_role)

    def test_create_user_multiple_roles(self):
        FlaskularUser.create('test1@example.com', 'test1',
                             roles=['admin', 'subscriber'])

        found = FlaskularUser.query.filter_by(
            email='test1@example.com'
        ).first()
        assert found is not None
        assert found.email == 'test1@example.com'
        assert verify_password('test1', found.password)

        admin_role = FlaskularRole.query.filter_by(name='admin').first()
        sub_role = FlaskularRole.query.filter_by(name='subscriber').first()

        assert found.has_role(admin_role)
        assert found.has_role(sub_role)

    def test_create_user_no_store(self):
        user = FlaskularUser.create('test2@example.com', 'test2', 'admin',
                                    commit=False)

        assert user.email == 'test2@example.com'
        assert verify_password('test2', user.password)

        admin_role = FlaskularRole.query.filter_by(name='admin').first()
        sub_role = FlaskularRole.query.filter_by(name='subscriber').first()

        assert user.has_role(admin_role)
        assert not user.has_role(sub_role)

        found = FlaskularUser.query.filter_by(
            email='test2@example.com'
        ).first()
        assert found is None

    def test_login_success(self):
        """
        Tests to make sure a login succeeds without error. To check to see if
        the user actually got logged in, the test case `test_get_current_user`
        is used instead.
        """
        user = FlaskularUser.login('admin@example.com', 'admin')
        assert user is not None
        assert len(user.roles) == 1
        assert user.roles[0].name == 'admin'

    def test_get_current_user(self):
        user = FlaskularUser.query.filter_by(email='admin@example.com').first()
        assert user.logged_out
        FlaskularUser.login(user.email, 'admin')

        found = FlaskularUser.get_current_user()
        assert found == user
        assert not found.logged_out
        assert len(found.roles) == 1
        assert found.roles[0].name == 'admin'

    def test_get_current_user_none_logged_in(self):
        assert FlaskularUser.get_current_user() is None

    def test_second_login(self):
        # should work
        FlaskularUser.login('admin@example.com', 'admin')
        token = FlaskularUser.login('admin@example.com', 'admin')
        assert token is not None

    def test_dual_login(self):
        FlaskularUser.create('bogus@example.com', 'bogus')
        first = FlaskularUser.login('bogus@example.com', 'bogus')
        first_token = first.get_auth_token()
        assert FlaskularUser.get_from_token(first_token) is not None

        second = FlaskularUser.login('admin@example.com', 'admin')
        second_token = second.get_auth_token()
        assert FlaskularUser.get_from_token(second_token) is not None

        with pytest.raises(err.AuthTokenLoggedOut):
            # Make sure dual login forced the first user to log out
            FlaskularUser.get_from_token(first_token)

    def test_login_user_not_found(self):
        with pytest.raises(err.AuthUserNotFound) as excinfo:
            FlaskularUser.login('bogus@example.com', 'bogus')

        error = excinfo.value
        assert error.user_id == 'bogus@example.com'

    def test_login_invalid_password(self):
        with pytest.raises(err.AuthInvalidPassword):
            FlaskularUser.login('admin@example.com', 'bogus')

    def test_logout(self):
        FlaskularUser.login('admin@example.com', 'admin')
        user = FlaskularUser.get_current_user()
        assert not user.logged_out
        assert user.email == 'admin@example.com'

        email = FlaskularUser.logout()
        assert email == user.email
        assert FlaskularUser.get_current_user() is None

        found = FlaskularUser.query.filter_by(
            email='admin@example.com'
        ).first()
        assert found.logged_out

    def test_logout_nobody(self):
        with pytest.raises(err.AuthLogoutNobody):
            FlaskularUser.logout()

    def test_logout_twice(self):
        FlaskularUser.login('admin@example.com', 'admin')
        FlaskularUser.logout()
        with pytest.raises(err.AuthLogoutNobody):
            FlaskularUser.logout()

    def test_set_password(self):
        user = FlaskularUser.query.filter_by(email='admin@example.com').first()
        user.set_password('changed')

        found = FlaskularUser.query.filter_by(
            email='admin@example.com'
        ).first()
        assert user == found
        assert verify_password('changed', user.password)

    def test_reset_password(self):
        user = FlaskularUser.query.filter_by(email='admin@example.com').first()
        user.reset_password('admin', 'changed')

        found = FlaskularUser.query.filter_by(
            email='admin@example.com'
        ).first()
        assert user == found
        assert verify_password('changed', user.password)

    def test_reset_password_fails(self):
        user = FlaskularUser.query.filter_by(email='admin@example.com').first()

        with pytest.raises(err.NotAuthorized):
            user.reset_password('bogus', 'changed')

    def test_fail_user_from_token(self):
        with pytest.raises(BadSignature):  # Expired token tested in controller
            FlaskularUser.get_from_token('A bogus token.')

    def test_no_user_from_token(self):
        """
        Corner case; one way it can occur if a login is attempted with a
        user that has been created but not stored in the database.
        """
        user = FlaskularUser.create('test3@example.com', 'test3', 'subscriber',
                                    commit=False)

        with pytest.raises(err.AuthTokenNoUser):
            token = user.get_auth_token()
            FlaskularUser.get_from_token(token)

    def test_login_token(self):
        loggedin = FlaskularUser.login('admin@example.com', 'admin')
        assert loggedin is not None

        token = loggedin.get_auth_token()
        assert token is not None

        user = FlaskularUser.get_from_token(token)

        assert user is not None
        assert user.email == 'admin@example.com'
        assert len(user.roles) == 1
        assert user.roles[0].name == 'admin'

    def test_login_logout_token_fails(self):
        loggedin = FlaskularUser.login('admin@example.com', 'admin')
        assert loggedin is not None

        token = loggedin.get_auth_token()
        assert token is not None

        user = FlaskularUser.get_from_token(token)

        assert user is not None
        assert user.email

        FlaskularUser.logout()

        with pytest.raises(err.AuthTokenLoggedOut):
            FlaskularUser.get_from_token(token)

    def test_wrong_password_in_token(self):
        loggedin = FlaskularUser.login('admin@example.com', 'admin')
        token = loggedin.get_auth_token()
        user = FlaskularUser.get_from_token(token)

        # Invalidates the password in the token
        user.set_password('changed')

        with pytest.raises(err.AuthTokenInvalid):
            FlaskularUser.get_from_token(token)

    def test_update_profile(self):
        user = FlaskularUser.query.filter_by(email='admin@example.com').first()
        user.update_profile(first_name='The', last_name='Admin')

        redo = FlaskularUser.query.filter_by(email='admin@example.com').first()
        assert redo.first_name == 'The'
        assert redo.last_name == 'Admin'

    def test_promote_demote(self):
        FlaskularUser.create('test1@example.com', 'test1', 'subscriber')
        user = FlaskularUser.query.filter_by(email='test1@example.com').first()

        admin_role = FlaskularRole.query.filter_by(name='admin').first()
        subscriber_role = FlaskularRole.query.filter_by(
            name='subscriber'
        ).first()

        def _assert_subscriber_only(user):
            assert user.has_role(subscriber_role)
            assert not user.has_role(admin_role)

        def _assert_admin_only(user):
            assert user.has_role(admin_role)
            assert not user.has_role(subscriber_role)

        # Make sure new user is only subscriber
        _assert_subscriber_only(user)

        # Promote user
        user.promote()
        _assert_admin_only(user)

        # Make sure promoting an admin doesn't do anything (bad or otherwise)
        user.promote()
        _assert_admin_only(user)

        # Demote user
        user.demote()
        _assert_subscriber_only(user)

        # Make sure demoting a subscriber doesn't do anything (bad or
        # otherwise)
        user.demote()
        _assert_subscriber_only(user)

    def test_delete_user(self):
        FlaskularUser.create('test1@example.com', 'test1', 'subscriber')
        user = FlaskularUser.query.filter_by(email='test1@example.com').first()

        assert user is not None
        user.delete()

        user = FlaskularUser.query.filter_by(email='test1@example.com').first()
        assert user is None

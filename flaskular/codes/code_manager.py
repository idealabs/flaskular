from functools import partial

from . import Code


class CodeManager(object):
    """
    Manager for defining and using server codes. These codes store messages and
    unambiguous names for communicating with the client and allowing the client
    to react based on the server's response.

    Example Usage
    -------------

            # code definitions
            code_dict = {
                'example_success': 'The server succeeded',
                'example_failed': ('We failed. Expected "{expected}", ' +
                                   'got "{actual}".')
            }
            codes = CodeManager(code_dict)

            # code usage
            cs = codes.example_success()
            cs.name == 'example_success'                        # True
            cs.msg == 'The server succeeded'                    # True

            cf = codes.example_failed('exp', 'act')
            cf.name == 'example_failed'                         # True
            cf.msg == 'We failed. Expected "exp", got "act".'   # True

    Methods
    -------
    [auto] : function (list of str -> Code)
        Function name [auto] is auto-generated and is a key from parameter
        ()`code_dict`.

        The function takes as parameters a (possibly empty) list of strings.
        These strings are inserted into the wildcards created in the message
        of the code defined by `code_dict`.

        The function returns a Code object with the wildcards of the message
        filled.

    Parameters
    ----------
    code_dict : dictionary
        The dictionary used by the manager to create all the codes. The keys
        are the (unambigious) code names and the values are the messages.

        The messages must name the wildcards to fill, so that code_dict is
        self documenting.
    """

    def __init__(self, code_dict):

        for name, msg_partial in code_dict.items():

            def build_code(name='', msg_partial='', **kwargs):
                return Code(name, msg_partial, **kwargs)

            f = partial(build_code, name=name, msg_partial=msg_partial)
            setattr(self, name, f)

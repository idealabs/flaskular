from flask import Flask
from flask.app import _endpoint_from_view_func, setupmethod


class Flaskular(Flask):
    """Extends Flask, allowing routes to be overwritten when desired.

    Otherwise used in the same way.
    """

    overwritable_views = {}

    @setupmethod
    def add_url_rule(self, rule, endpoint=None, view_func=None, **options):
        """Adds a url rule that can be overwritten.

        Identical in specs to :meth:`Flask.add_url_rule` except that it can
        also take an `overwritable` parameter. If given, the view can
        be overwritten by another with the same endpoint.

        Note that overwriting a view resets the ability for that view to be
        overwritten. Either the overwriting view has to also pass
        `overwritable = True` or the new view is not overwritable.

        Parameters
        ----------
        [see :meth:`Flask.add_url_url`]
        rule : str
        endpoint : str or None
        view_func : function
        overwritable : Boolean
            True if the endpoint can be overwritten.
        options : dict (keyword arguments)
        """
        if endpoint is None:
            endpoint = _endpoint_from_view_func(view_func)
        overwritable = options.pop('overwritable', False)

        if (endpoint in self.overwritable_views and
                endpoint in self.view_functions):
            self.view_functions.pop(endpoint)

            if not overwritable:
                # Reset overwritable, don't overwrite if overwriting function
                # is not also overwritable
                self.overwritable_views.pop(endpoint)

        if overwritable:
            self.overwritable_views[endpoint] = True

        Flask.add_url_rule(self, rule, endpoint, view_func, **options)

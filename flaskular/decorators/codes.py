"""
Defines the codes and messages which are used by the server to respond to the
client.
"""
from flaskular.codes import CodeManager


# Keys are the code id, values are the code message
_code_dict = {
    # Unknown,
    'unknown': 'A critical unknown error has occured (see server logs)',

    # JSON Post
    'request_not_json': 'The request was not a json request.',
    'request_missing_argument': ('The request is missing the following ' +
                                 'required argument: {arg}'),

    # Auth required
    'request_login_required': 'Login required',
    'request_token_required': 'Login required, token not given',
    'request_invalid_token': 'Login failed: invalid token',
    'request_expired_token': 'Login failed: expired token',

    # Roles required
    'request_role_required': 'Not authenticated: user requires role "{role}"',

    # Roles accepted
    'request_roles_not_accepted': ('Not authenticated: user does not have ' +
                                   'sufficient privileges')
}

# Auto-register codes
AUTH_CODES = CodeManager(_code_dict)

import json

from flask import request

from flaskular.test_base import TestController
from flaskular.codes import CodeManager
from . import json_post


class TestJSONPost(TestController):

    def setUp(self):
        TestController.setUp(self)
        self.c = CodeManager({
            'success_code': 'A code'
        })

    def test_success(self):

        app = self.app
        url = '/test'

        @app.route(url, methods=['POST'])
        @json_post()
        def fcn():
            return {
                'code': self.c.success_code()
            }

        data, status = self.post(url)

        assert status == 200
        assert data['success']
        assert data['code'] == 'success_code'

    def test_success_args(self):

        app = self.app
        url = '/test'

        @app.route(url, methods=['POST'])
        @json_post('arg1', 'arg2')
        def fcn():
            return {
                'code': self.c.success_code(),
                'arg1': request.json['arg1'],
                'arg2': request.json['arg2']
            }

        data, status = self.post(url, arg1='a', arg2='b')

        assert status == 200
        assert data['success']
        assert data['code'] == 'success_code'
        assert data['arg1'] == 'a'
        assert data['arg2'] == 'b'

    def test_not_json(self):

        app = self.app
        url = '/test'

        @app.route(url, methods=['POST'])
        @json_post()
        def fcn():
            return {
                'code': self.c.success_code()  # Shouldn't see
            }

        response = self.client.post(url, follow_redirects=True)
        status = response.status_code
        data = json.loads(response.data.decode('utf-8'))

        assert status == 200
        assert not data['success']
        assert data['code'] == 'request_not_json'

    def test_missing_arg(self):
        app = self.app
        url = '/test'

        @app.route(url, methods=['POST'])
        @json_post('arg1', 'arg2')
        def fcn():
            return {
                'code': self.c.success_code(),
                'arg1': request.json['arg1'],
                'arg2': request.json['arg2']
            }

        data, status = self.post(url, arg1='a')

        assert status == 200
        assert not data['success']
        assert data['code'] == 'request_missing_argument'
        assert 'arg2' in data['msg']

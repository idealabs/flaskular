from flask_sqlalchemy import SQLAlchemy
from flask_security import Security

from colored import fg, bg, attr

db = SQLAlchemy()
security = Security()


def print_warning(msg):
    print('{}{}{}{}'.format(fg('black'), bg('yellow'), msg, attr('reset')))


def print_error(msg):
    print('{}{}{}{}'.format(fg('white'), bg('red'), msg, attr('reset')))


def print_ok(msg):
    print('{}{}{}'.format(fg('green'), msg, attr('reset')))


class RegistrationManager:
    """Manages registration of app classes to be used by Flaskular.
    """

    User = None
    Role = None

/**
 * Service for authentication, connecting to the flask server.
 *
 * @namespace flaskular.auth
 */
(function() {
    'use strict';

    angular
        .module('flaskular.auth')
        .service('authService', authService);

    authService.$inject = ['$q', '$timeout', '$http', '$cookies'];

    /**
     * Auth service definition.
     */
    function authService($q, $timeout, $http, $cookies) {

        // The user
        var user = false;

        // Available functions for use in controllers
        var service = {
            post: post,
            postHeaderAuth: postHeaderAuth,
            loginIfCurrent: loginIfCurrent,
            isLoggedIn: isLoggedIn,
            getCurrentUser: getCurrentUser,
            login: login,
            tokenLogin: tokenLogin,
            logout: logout,
            register: register,
            hasRole: hasRole,
            hasAllRoles: hasAllRoles,
            hasOneRole: hasOneRole,
            getUserList: getUserList,
            getUserDict: getUserDict,
            changePassword: changePassword,
            updateProfile: updateProfile,
            promoteUser: promoteUser,
            demoteUser: demoteUser,
            deleteUser: deleteUser
        };

        return service;

        //////////////////////////////////////
        //  Implementations
        //////////////////////////////////////

        /**
         * Runs a post using the user's authentication token.
         *
         * The post is successful if and only if the server returns with
         * status 200 and a json response containing key `success` with
         * value `true`.
         *
         * @param {str (url)} url The url to which the post is made.
         * @param {object (json serializable)} postData The data to post as a
         * json object to the url.
         *
         * @returns {promise} promise The promise.
         *
         * Usage
         * -----
         * With custom handlers in the service.
         *
         *      function myServiceFunction() {
         *          var deferred = $q.defer();
         *
         *          // Handle for a successful post
         *          function handleSuccess(data, status) {
         *              // Custom success resolution scripts here
         *              // ...
         *              deferred.resolve(data, status);
         *          }
         *
         *          // Handle for a failed post
         *          function handleError(data, status) {
         *              // Custom failure resolution scripts here
         *              // ...
         *              deferred.reject(data, status);
         *          }
         *
         *          // Make the post
         *          authService.post('some/url', {some: data})
         *              .then(handleSuccess)
         *              .catch(handleError);
         *
         *          // Return the promise
         *          return deferred.promise;
         *      }
         *
         * Since the service returns a promise to the controller, if no custom
         * service success/error handlers are needed, then the post promise
         * can be simply returned directly. The following example will do the
         * same thing as above, supposing nothing was added to the handlers:
         *
         *      function myServiceFunction() {
         *          // Make the post
         *          return authService.post('some/url', {some: data});
         *      }
         *
         */
        function post(url, postData) {
            var deferred = $q.defer();

            if (postData === undefined) {
                postData = {};
            }

            postData.token = $cookies.getObject('token');

            // Handle for a successful post request
            function handleSuccess(data, status) {
                if (status === 200 && data.success) {
                    deferred.resolve(data, status);
                }
                else {
                    handleError(data, status);
                }
            }

            // Handle for a failed post request
            function handleError(data, status) {
                deferred.reject(data, status);
            }

            // Send the request
            $http.post(url, postData)
                .success(handleSuccess)
                .error(handleError);

            // Return the promise
            return deferred.promise;
        }

        /**
         * Runs a POST by authenticating with the user's token
         * in the header rather than the body. This can be used if there
         * needs to be a POST request that cannot modify the data object given,
         * like in a FormData object that comes from an HTML form upload
         *
         * The post is successful if and only if the server returns with
         * status 200 and a json response containing key `success` with
         * value `true`.
         *
         * @param {str (url)} url The url to which the post is made.
         * @param {object} postData - The data object to be uploaded
         * to the url.
         * @param {object} headers - Optional - Any extra headers that need to
         * be added to the request. This may be left as 'undefined'
         *
         *
         * @returns {promise} promise The promise.
         *
         * Usage
         * -----
         * With custom handlers in the service.
         *
         *      function myServiceFunction(formData) {
         *          var deferred = $q.defer();
         *
         *          // Handle for a successful post
         *          function handleSuccess(data, status) {
         *              // Custom success resolution scripts here
         *              // ...
         *              deferred.resolve(data, status);
         *          }
         *
         *          // Handle for a failed post
         *          function handleError(data, status) {
         *              // Custom failure resolution scripts here
         *              // ...
         *              deferred.reject(data, status);
         *          }
         *
         *          var headers = {'Content-Type': undefined};
         *
         *          // Make the post
         *          authService.postHeaderAuth('some/url', postData, headers)
         *              .then(handleSuccess)
         *              .catch(handleError);
         *
         *          // Return the promise
         *          return deferred.promise;
         *      }
         *
         * Since the service returns a promise to the controller, if no custom
         * service success/error handlers are needed, then the post promise
         * can be simply returned directly. The following example will do the
         * same thing as above, supposing nothing was added to the handlers
         * (Note that in this example, the headers are not passed in):
         *
         *      function myServiceFunction(postData) {
         *          // Make the post
         *          return authService.postHeaderAuth('some/url', postData);
         *      }
         *
         */
        function postHeaderAuth(url, postData, headers) {
            var deferred = $q.defer();

            // Create headers if not already defined
            if (headers === undefined) {
                headers = {};
            }

            // Add user token to headers
            headers.token = $cookies.getObject('token');

            // Handle for a successful post request
            function handleSuccess(data, status) {
                if (status === 200 && data.success) {
                    deferred.resolve(data, status);
                }
                else {
                    handleError(data, status);
                }
            }

            // Handle for a failed post request
            function handleError(data, status) {
                deferred.reject(data, status);
            }

            // Send the request
            $http.post(url, postData, {
                headers: headers
            })
                .success(handleSuccess)
                .error(handleError);

            // Return the promise
            return deferred.promise;
        }

        /**
         * Logs the user in if the user is current for the server session.
         *
         * Used to initialize the app on a page refresh.
         */
        function loginIfCurrent() {
            var deferred = $q.defer();

            function handleSuccess(data, status) {
                user = data.user;
                $cookies.putObject('token', data.token);
                deferred.resolve(data);
            }

            function handleError(data, status) {
                user = false;
                $cookies.remove('token');
                deferred.reject(data);
            }

            service.post('/auth/token-login')
                .then(handleSuccess)
                .catch(handleError);

            return deferred.promise;
        }

        /**
         * Returns whether or not the user is logged in.
         *
         * @returns {bool} true if logged in, false otherwise.
         */
        function isLoggedIn() {
            if (user) {
                return true;
            }
            else {
                return false;
            }
        }

        /**
         * Returns the id (email address) of the current user, or null if
         * not logged in.
         *
         * @returns {str or false} The id (email) address of the current user
         * or null if no such user exists.
         */
        function getCurrentUser() {
            if (!isLoggedIn()) {
                return false;
            }
            return user;
        }

        /**
         * Returns true if the user has the given role, false otherwise.
         *
         * @param {str} role The role to check.
         *
         * @returns {bool} True if the user has the role, false if it doesn't
         * or if no user is logged in.
         */
        function hasRole(role) {
            if(!user) {
                return false;
            }
            return (user.roles.indexOf(role) > -1);
        }

        /**
         * Returns true if the user has all of the roles in the given list.
         *
         * @param {list of str or undefined or false} roles The list of roles
         * to check.
         *
         * @return {bool} True if the user has all roles in the given list of
         * roles. Returns true if the list is undefined or false.
         */
        function hasAllRoles(roles) {
            if (!roles) {
                return true;
            }
            for (var i = 0; i < roles.length; i++) {
                var role = roles[i];
                if (!service.hasRole(role)) {
                    return false;
                }
            }
            return true;
        }

        /**
         * Returns true if the user has at least one role in the given list.
         *
         * @param {list of str or undefined or false} roles The list of roles
         * to check.
         *
         * @return {bool} True if the user has at least one of the roles in
         * the list of roles, false otherwise. Returns true if the list of
         * roles is undefined or false.
         */
        function hasOneRole(roles) {
            if (!roles) {
                return true;
            }
            for (var i = 0; i <= roles.length; i++) {
                var role = roles[i];
                if (service.hasRole(role)) {
                    return true;
                }
            }
            return false;
        }

        /**
         * Attempts to login the user with the given email and password.
         *
         * @param {string} email The user's email.
         * @param {string} password The user's password.
         *
         * @returns {promise} A promise to perform the login logic.
         */
        function login(email, password) {
            var deferred = $q.defer();

            // Handle for successful login
            function handleSuccess(data, status) {
                if (status === 200 && data.success) {
                    user = data.user;
                    $cookies.putObject('token', data.token);
                    deferred.resolve(data);
                }
                else {
                    handleError(data, status);
                }
            }

            // Handle for failed login
            function handleError(data, status) {
                user = false;
                $cookies.remove('token');

                if (status !== 200) {
                    // In case other status messages are received, make sure
                    // data is created with a code and a message for the
                    // system to understand.
                    data = {
                        code: 'critical_server_error',
                        msg: 'A critical server error has occured',
                        status: status,
                        success: false,
                        returned: data
                    };
                }

                deferred.reject(data);
            }

            // Send a post request to the server
            $http.post('/auth/login', {email: email, password: password})
                .success(handleSuccess)
                .error(handleError);

            // Return the promise
            return deferred.promise;
        }

        /**
         * Attempts to login the user with the given token.
         *
         * @param {str} token The token to use to attempt the login.
         *
         * @returns {promise} A promise to perform the token login logic.
         */
        function tokenLogin(token) {
            var deferred = $q.defer();

            // Handle for successful login
            function handleSuccess(data, status) {
                if (status === 200 && data.success) {
                    user = data.user;
                    $cookies.putObject('token', data.token);
                    deferred.resolve(data);
                }
                else {
                    handleError(data, status);
                }
            }

            // Handle for failed login
            function handleError(data, status) {
                user = false;
                $cookies.remove('token');

                if (status !== 200) {
                    // In case other status messages are received, make sure
                    // data is created with a code and a message for the
                    // system to understand.
                    data = {
                        code: 'critical_server_error',
                        msg: 'A critical server error has occured',
                        status: status,
                        success: false,
                        returned: data
                    };
                }
                deferred.reject(data);
            }

            $http.post('/auth/token-login', {token: token})
                .success(handleSuccess)
                .error(handleError);

            return deferred.promise;
        }

        /**
         * Attempts to log the current user out.
         *
         * @returns {promise} A promise to perform the logout logic.
         */
        function logout() {
            var deferred = $q.defer();

            // Handle for successful logout
            function handleSuccess(data, status) {
                if (status === 200 && data.success) {
                    user = false;
                    $cookies.remove('token');
                    deferred.resolve(data);
                }
                else {
                    handleError(data, status);
                }
            }

            // Handle for failed logout, logout front anyway
            function handleError(data, status) {
                user = false;
                $cookies.remove('token') ;
                deferred.reject(data);
            }

            // Send a get request to the server
            $http.get('/auth/logout')
                .success(handleSuccess)
                .error(handleError);

            // Return the promise
            return deferred.promise;
        }

        /**
         * Attempts to register a new user.
         *
         * @param {str} email The email to use as the alias for the new user.
         * @param {str} password The new user's password.
         *
         * @returns {promise} The promise to register.
         */
        function register(email, password) {
            var deferred = $q.defer();

            // Handle for successful registration
            function handleSuccess(data, status) {
                if (status === 200 && data.success) {
                    deferred.resolve(data);
                }
                else {
                    handleError(data, status);
                }
            }

            // Handle for failed registration
            function handleError(data, status) {
                deferred.reject(data);
            }

            // Send a post request to the server
            $http.post('/auth/register', {email: email, password: password})
                .success(handleSuccess)
                .error(handleError);

            // Return the promise
            return deferred.promise;
        }

        /**
         * Attempts to get a list of all users.
         */
        function getUserList() {
            var deferred = $q.defer();

            // Handle for a successful query
            function handleSuccess(data, status) {
                deferred.resolve(data);
            }

            // Handle for failed query
            function handleError(data, status) {
                deferred.reject(data);
            }

            // Send the request to the server
            service.post('/auth/user-list')
                .then(handleSuccess)
                .catch(handleError);

            // Return the promise
            return deferred.promise;
        }

        /**
         * Gets the user dictionary for the current logged in user.
         */
        function getUserDict() {
            var deferred = $q.defer();

            // Handle for a successful query
            function handleSuccess(data, status) {
                deferred.resolve(data);
            }

            // Handle for a failed query
            function handleError(data, status) {
                deferred.reject(data);
            }

            // Send the request to the server
            service.post('/auth/user-dict')
                .then(handleSuccess)
                .catch(handleError);

            // Return the promise
            return deferred.promise;
        }

        /**
         * Changes a user's password, checking to see if the old password
         * supplied is correct.
         *
         * @param {int} id The user id.
         * @param {str} oldPassword The old (and current) password
         * @param {str} newPassword The new password (to change to)
         */
        function changePassword(id, oldPassword, newPassword) {
            var deferred = $q.defer();
            var json = {
                id: id,
                oldPassword: oldPassword,
                newPassword: newPassword
            };

            // Handle for a successful query
            function handleSuccess(data, status) {
                deferred.resolve(data);
            }

            // Handle for a failed query
            function handleError(data, status) {
                deferred.reject(data);
            }

            // Send the request to the server.
            service.post('/auth/change-password', json)
                .then(handleSuccess)
                .catch(handleError);

            // Return the promise
            return deferred.promise;
        }

        /**
         * Updates a user's profile.
         *
         * @param {int} id The user id.
         * @param {str} firstName
         * @param {str} lastName
         */
        function updateProfile(id, firstName, lastName) {
            var deferred = $q.defer();
            var json = {
                id: id,
                firstName: firstName,
                lastName: lastName
            };

            // Handle for a successful query
            function handleSuccess(data, status) {
                deferred.resolve(data);
            }

            // Handle for a failed query
            function handleError(data, status) {
                deferred.reject(data);
            }

            // Send the request to the server
            service.post('/auth/update-profile', json)
                .then(handleSuccess)
                .catch(handleError);

            // Return the promise
            return deferred.promise;
        }

        /**
         * Deletes a user with the given id.
         *
         * @param {int} id The user id.
         */
        function deleteUser(id) {
            var deferred = $q.defer();

            // Handle for a successful query
            function handleSuccess(data, status) {
                deferred.resolve(data);
            }

            // Handle for a failed query
            function handleError(data, status) {
                deferred.reject(data);
            }

            // Send the request to the server
            service.post('/auth/delete-user', {id: id})
                .then(handleSuccess)
                .catch(handleError);

            // Return the promise
            return deferred.promise;
        }

        /**
         * Promotes a user with the given id to an admin.
         *
         * @param {int} id The user id.
         */
        function promoteUser(id) {
            var deferred = $q.defer();

            // Handle for a successful query
            function handleSuccess(data, status) {
                deferred.resolve(data);
            }

            // Handle for a failed query
            function handleError(data, status) {
                deferred.reject(data);
            }

            // Send the request to the server
            service.post('/auth/promote-user', {id: id})
                .then(handleSuccess)
                .catch(handleError);

            // Return the promise
            return deferred.promise;
        }

        /**
         * Demotes a user with the given id from an admin.
         *
         * @param {int} id The user id.
         */
        function demoteUser(id) {
            var deferred = $q.defer();

            // Handle for a successful query
            function handleSuccess(data, status) {
                deferred.resolve(data);
            }

            // Handle for a failed query
            function handleError(data, status) {
                deferred.reject(data);
            }

            // Send the request to the server
            service.post('/auth/demote-user', {id: id})
                .then(handleSuccess)
                .catch(handleError);

            // Return the promise
            return deferred.promise;
        }

    }
})();

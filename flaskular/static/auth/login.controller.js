/**
 * The login controller.
 *
 * @namespace flaskular.auth
 */
(function() {
    'use strict';

    angular
        .module('flaskular.auth')
        .controller('LoginController', LoginController);

    LoginController.$inject = ['$state', 'authService'];

    ///////////////////////////////////////////////////////////////////////////

    /**
     * Login controller definition.
     */
    function LoginController($state, authService) {
        var vm = this;

        vm.login = login;


        /////////////////////////////////////////
        //  Implementations
        /////////////////////////////////////////

        // TODO: redirect if already logged in

        /**
         * Attempts a login using the information in the login form.
         */
        function login() {
            vm.error = false;
            vm.disabled = true;

            // Handle for a successful login
            function handleSuccess(data) {
                $state.go('home');
                vm.disabled = false;
                vm.loginForm = {};
                vm.error = false;
            }

            // Handle for a failed login
            function handleError(data) {
                vm.error = true;
                vm.errorMessage = data.msg;

                if (vm.errorMessage === undefined ||
                        vm.errorMessage.trim() === '') {
                    // Make sure some data comes in.
                    data.code = 'unknown';
                    data.msg = 'An unknown client-side error has occured';
                    vm.errorMessage = data.msg;
                }

                if (data.code === 'login_invalid_password') {
                    vm.loginForm = {
                        email: vm.loginForm.email
                    };
                }
                else if (data.code === 'login_unknown_user') {
                    vm.loginForm = {};
                }
                else {
                    vm.loginForm = {};
                    $state.go('error', data, {});
                }

                vm.disabled = false;

            }

            authService.login(vm.loginForm.email, vm.loginForm.password)
                .then(handleSuccess)
                .catch(handleError);

        }
    }
})();

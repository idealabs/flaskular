(function() {
    'use strict';

    describe('RegisterController', function() {

        var vm;
        var $provide;
        var stateStub;
        var authServiceStub;
        var q;
        var testScope;
        var currState;

        beforeEach(module('flaskular.auth'));

        beforeEach(module(function($urlRouterProvider) {
            $urlRouterProvider.deferIntercept();
        }));

        beforeEach(inject(function($rootScope, $controller, $q) {

            q = $q;
            testScope = $rootScope.$new();

            currState = 'auth/register';
            stateStub = {
                go: sinon.spy(function(arg) {
                    currState = arg;
                }),
            };

            authServiceStub = {
                register: sinon.stub(),
                isLoggedIn: sinon.stub()
            };

            vm = $controller('RegisterController', {
                '$state': stateStub,
                'authService': authServiceStub
            });
        }));

        ///////////////////////////////////////////////////////////////////////
        //  Helpers
        ///////////////////////////////////////////////////////////////////////

        /**
         * Runs a register attempt.
         *
         * @param {str} code The code returned by the authService.
         * @param {str} msg The message returned by the authService.
         * @param {str} email The email to place into the register form.
         * @param {str} password The password to place into the register form.
         * @param {bool} success True if the authService resolves, false if
         * it rejects.
         */
        function runRegister(code, msg, email, password, success) {
            var expected = {
                code: code,
                msg: msg
            };
            var defer = q.defer();

            if (success) {
                defer.resolve(expected);
            }
            else {
                defer.reject(expected);
            }

            authServiceStub.register.withArgs(email, password)
                .returns(defer.promise);
            authServiceStub.register
                .throws();

            vm.registerForm = {
                email: email,
                password: password
            };

            vm.register();
            testScope.$apply();

            expect(vm.disabled).toBe(false);
        }

        ///////////////////////////////////////////////////////////////////////
        //  Tests
        ///////////////////////////////////////////////////////////////////////

        //----------------
        //  Register Tests
        //----------------

        it('should register a user', function() {
            runRegister('register_success', 'Register successful',
                        'test@example.com', 'test', true);

            expect(vm.registerForm).toEqual({});
            expect(currState).toEqual('auth/login');
        });

        it('should fail to register a user', function() {
            runRegister('register_already_exists', 'User already exists',
                        'test@example.com', 'test', false);

            expect(vm.registerForm).toEqual({});
            expect(currState).toEqual('auth/register');
            expect(vm.error).toBe(true);
            expect(vm.errorMessage).toBe('User already exists');
        });

        it('should fail to register with an error', function() {
            runRegister('register_bogus_code', 'something went wrong',
                        'test@example.com', 'test', false);

            expect(vm.registerForm).toEqual({});
            expect(currState).toEqual('error');
            expect(vm.error).toBe(true);
            expect(vm.errorMessage).toBe('something went wrong');
        });

        it('should create a client error', function() {
            runRegister('', '', '', '', false);

            expect(vm.registerForm).toEqual({});
            expect(currState).toEqual('error');
            expect(vm.error).toBe(true);
            expect(vm.errorMessage).toBe(
                'An unknown client-side error has occured');
        });

        it('should also create a client error', function() {
            runRegister('', '', '', '', false);

            expect(vm.registerForm).toEqual({});
            expect(currState).toEqual('error');
            expect(vm.error).toBe(true);
            expect(vm.errorMessage).toBe(
                'An unknown client-side error has occured');
        });
    });
})();

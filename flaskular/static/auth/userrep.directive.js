/**
 * Directive for displaying a user.
 *
 * If the user has a name (first and/or last) defined then the display is
 *
 *      [first] [last] ([email])
 *
 *      John Doe (jd@example.com)
 *
 * If the user doesn't have a name, then the display is
 *
 *      [email]
 *
 *      jd@example.com
 *
 * Usage
 * -----
 *
 *      <user-rep user='user'></user-rep>
 *
 * Attributes
 * ----------
 * user : obj
 *  The user object. Must have key 'user'. Keys 'firstName' and 'lastName' are
 *  also checked.
 *
 * @namespace app.auth)
 */
(function() {
    'use strict';

    angular
        .module('flaskular.auth')
        .directive('userRep', userRep);

    ///////////////////////////////////////////////////////////////////////////

    function userRep() {
        return {
            restrict: 'E',
            template: '{{name}}',
            scope: {
                'user': '='
            },
            link: function(scope, element, attrs) {

                scope.$watch(function(){
                    return scope.user;
                }, function(changedTo, changedFrom) {
                    initialize(changedTo);
                }, true);

                function initialize(user) {
                    if (_isEmpty(user.firstName) && _isEmpty(user.lastName)) {
                        scope.name = user.email;
                    }
                    else if (_isEmpty(user.firstName)) {
                        scope.name = user.lastName + ' (' + user.email + ')';
                    }
                    else if (_isEmpty(user.lastName)) {
                        scope.name = user.firstName + ' (' + user.email + ')';
                    }
                    else {
                        scope.name = user.firstName + ' ' + user.lastName +
                            ' (' + user.email + ')';
                    }
                }

                function _isEmpty(part) {
                    return (part === undefined || part === null ||
                            part.trim() === '');
                }
            }
        };
    }
})();

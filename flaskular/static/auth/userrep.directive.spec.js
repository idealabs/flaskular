(function() {
    'use strict';

    describe('userRep Directive', function() {

        var compile;
        var testScope;

        beforeEach(module('flaskular.auth'));

        beforeEach(inject(function($rootScope, $compile) {
            testScope = $rootScope.$new();
            compile = $compile;
        }));

        ///////////////////////////////////////////////////////////////////////
        // Tests
        ///////////////////////////////////////////////////////////////////////

        it ('should be email only', function() {
            var user = {
                email: 'admin@example.com'
            };
            var html = buildRep(user, testScope, compile);

            expect(html).toEqual('admin@example.com');
        });

        it ('should be email only with empty string names', function() {
            var user = {
                email: 'admin@example.com',
                firstName: '',
                lastName: ''
            };
            var html = buildRep(user, testScope, compile);

            expect(html).toEqual('admin@example.com');
        });

        it ('should be email only with whitespace names', function() {
            var user = {
                email: 'admin@example.com',
                firstName: '  ',
                lastName: '     '
            };
            var html = buildRep(user, testScope, compile);

            expect(html).toEqual('admin@example.com');
        });

        it ('should be email only with null names', function() {
            var user = {
                email: 'admin@example.com',
                firstName: null,
                lastName: null
            };
            var html = buildRep(user, testScope, compile);

            expect(html).toEqual('admin@example.com');
        });

        it ('should have a first name', function() {
            var user = {
                email: 'admin@example.com',
                firstName: 'Test',
                lastName: null
            };
            var html = buildRep(user, testScope, compile);

            expect(html).toEqual('Test (admin@example.com)');
        });

        it ('should have a last name', function() {
            var user = {
                email: 'admin@example.com',
                firstName: null,
                lastName: 'User'
            };
            var html = buildRep(user, testScope, compile);

            expect(html).toEqual('User (admin@example.com)');
        });

        it ('should have a first and last name', function() {
            var user = {
                email: 'admin@example.com',
                firstName: 'Test',
                lastName: 'User'
            };
            var html = buildRep(user, testScope, compile);

            expect(html).toEqual('Test User (admin@example.com)');
        });

    });

    function buildRep(user, scope, compile) {
        var element = '<user-rep user="user"></user-rep>';
        scope.user = user;
        element = compile(element)(scope);
        scope.$digest();
        return element.html();
    }
})();

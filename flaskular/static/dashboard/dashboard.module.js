/**
 * Module definition for dashboard.
 *
 * @namespace flaskular.dashboard
 */
(function() {
    'use strict';

    angular
        .module('flaskular.dashboard', [
            'ui.router',
        ]);
})();

/**
 * The controller for the home page.
 *
 * @namespace flaskular.home
 */
(function() {
    'use strict';

    angular
        .module('flaskular.home')
        .controller('HomeController', HomeController);

    HomeController.$inject = ['$state', 'authService'];

    ///////////////////////////////////////////////////////////////////////////

    /**
     * Home controller definition.
     */
    function HomeController($state, authService) {
        var vm = this;

        // Static Data
        vm.interval = 5000;  // Milliseconds

        // Dynamic Data
        vm.loginForm = {};
        vm.loginError = false;
        vm.loginErrorMsg = '';

        vm.registerForm = {};
        vm.registerError = false;
        vm.registerErrorMsg = '';

        // Methods
        vm.initialize = initialize;
        vm.login = login;
        vm.closeLoginError = closeLoginError;
        vm.register = register;
        vm.closeRegisterError = closeRegisterError;

        // Extendability
        vm.extendTo = extendTo;

        // Initialization
        initialize();

        /////////////////////////////////////////////
        //  Implementations
        /////////////////////////////////////////////

        /**
         * Run on controller initialization.
         */
        function initialize() {
        }

        /**
         * Attempts to login using the information in the login form.
         */
        function login() {
            runLogin(vm.loginForm.email, vm.loginForm.password);
        }

        /**
         * Runs the login process.
         *
         * @param {str} email The email to use for the login.
         * @param {str} password The password to use for the login.
         */
        function runLogin(email, password) {
            vm.loginError = false;
            vm.disabled = true;

            // Handle for a successful login
            function handleSuccess(data) {
                $state.go('dashboard');
                vm.disabled = false;
                vm.loginForm = {};
                vm.loginError = false;
            }

            // Handle for a failed login
            function handleError(data) {
                vm.loginError = true;

                if (data.msg  === undefined || data.msg.trim() === '') {
                    data.code = 'unknown';
                    data.msg = 'An unknown client-side error has occured';
                }

                vm.loginErrorMsg = data.msg;

                if (data.code === 'login_invalid_password') {
                    vm.loginForm = {
                        email: vm.loginForm.email
                    };
                }

                else if (data.code === 'login_unknown_user') {
                    vm.loginForm = {};
                }

                else {
                    vm.loginForm = {};
                    $state.go('error', data, {});
                }

                vm.disabled = false;
            }

            authService.login(email, password)
                .then(handleSuccess)
                .catch(handleError);
        }

        /**
         * Closes the login error message.
         */
        function closeLoginError() {
            vm.loginError = false;
        }

        /**
         * Attempts to register a new user with information in the register
         * form.
         */
        function register() {
            vm.registerError = false;

            // Handle for a successful registration
            function handleSuccess(data) {
                var email = vm.registerForm.email;
                var password = vm.registerForm.password;

                vm.disabled = false;
                vm.registerError = false;
                vm.registerForm = {};

                runLogin(email, password);
            }

            // Handle for a failed registration
            function handleError(data) {
                vm.registerError = true;

                if (data.msg === undefined || data.msg.trim() === '') {
                    data.code = 'unknown';
                    data.msg = 'An unknown client-side error has occured';
                }

                vm.registerErrorMsg = data.msg;

                if (data.code === 'register_already_exists') {
                    vm.registerForm = {};
                }
                else if (data.code === 'register_mismatch_passwords') {
                    delete vm.registerForm.password;
                    delete vm.registerForm.confirmPassword;
                }
                else {
                    vm.registerForm = {};
                    $state.go('error', data, {});
                }

                vm.disabled = false;
            }

            // If passwords match, run registration
            if (vm.registerForm.password === vm.registerForm.confirmPassword) {
                authService.register(
                    vm.registerForm.email, vm.registerForm.password
                )
                    .then(handleSuccess)
                    .catch(handleError);
            }
            else {
                handleError({
                    code: 'register_mismatch_passwords',
                    msg: 'Passwords don\'t match'
                });
            }
        }

        /**
         * Closes the register error message.
         */
        function closeRegisterError() {
            vm.registerError = false;
        }

        /**
         * Makes the controller extendable.
         */
        function extendTo(child) {
            vm = angular.extend(child, vm);
            return vm;
        }
    }
})();

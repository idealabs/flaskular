(function() {
    'use strict';

    describe('HomeController', function() {

        var vm;
        var $provide;
        var stateStub;
        var authServiceStub;
        var q;
        var testScope;
        var currState;

        beforeEach(module('flaskular.home'));

        beforeEach(module(function($urlRouterProvider) {
            $urlRouterProvider.deferIntercept();
        }));

        beforeEach(inject(function($rootScope, $controller, $q) {

            q = $q;
            testScope = $rootScope.$new();

            currState = 'home';
            stateStub = {
                go: sinon.spy(function(arg) {
                    currState = arg;
                }),
            };

            authServiceStub = {
                login: sinon.stub(),
                register: sinon.stub(),
                logout: sinon.stub(),
                isLoggedIn: sinon.stub()
            };

            vm = $controller('HomeController', {
                '$state': stateStub,
                'authService': authServiceStub
            });
        }));

        ///////////////////////////////////////////////////////////////////////
        //  Helpers
        ///////////////////////////////////////////////////////////////////////

        /**
         * Runs a login attempt.
         *
         * @param {str} code The code returned by the authService.
         * @param {str} msg The message returned by the authService.
         * @param {str} email The email to place into the login form.
         * @param {str} password The password to place into the login form.
         * @param {bool} success True if the authService resolves, false if
         * it rejects.
         */
        function runLogin(code, msg, email, password, success) {
            var expected = {
                code: code,
                msg: msg
            };
            var defer = q.defer();

            if (success) {
                defer.resolve(expected);
            }
            else {
                defer.reject(expected);
            }

            authServiceStub.login.withArgs(email, password)
                .returns(defer.promise);
            authServiceStub.login
                .throws();

            vm.loginForm = {
                email: email,
                password: password
            };

            vm.login();
            testScope.$apply();

            expect(vm.disabled).toBe(false);
        }

        /**
         * Runs a register attempt.
         *
         * @param {str} code The code returned by the authService.
         * @param {str} msg The message returned by the authService.
         * @param {str} email The email to place into the register form.
         * @param {str} password The password to place into the register form.
         * @param {bool} success True if the authService resolves, false if
         * it rejects.
         */
        function runRegister(code, msg, email, password, confirmPassword,
                             success) {
            var expected = {
                code: code,
                msg: msg
            };
            var defer = q.defer();

            if (success) {
                defer.resolve(expected);
            }
            else {
                defer.reject(expected);
            }

            authServiceStub.login.withArgs(email, password)
                .returns(defer.promise);
            authServiceStub.login
                .throws();

            authServiceStub.register.withArgs(email, password)
                .returns(defer.promise);
            authServiceStub.register
                .throws();

            vm.registerForm = {
                email: email,
                password: password,
                confirmPassword: confirmPassword
            };

            vm.register();
            testScope.$apply();

            expect(vm.disabled).toBe(false);

            expect(authServiceStub.login.calledOnce).toBe(success);
        }

        ///////////////////////////////////////////////////////////////////////
        //  Tests
        ///////////////////////////////////////////////////////////////////////

        //-------------
        //  Login Tests
        //-------------

        describe('vm.login', function() {

            it('should log in a user', function() {
                runLogin('login_success', 'Login successful',
                         'real@example.com', 'real', true);

                expect(vm.loginForm).toEqual({});
                expect(currState).toEqual('dashboard');
            });

            it('should not login due to a bad email', function() {
                runLogin('login_unknown_user', 'Unknown user: <user>',
                         'fake@example.com', 'fake', false);

                expect(vm.loginForm).toEqual({});
                expect(vm.loginError).toBe(true);
                expect(vm.loginErrorMsg).toEqual('Unknown user: <user>');

                expect(currState).toEqual('home');

                // Close error and check
                vm.closeLoginError();
                expect(vm.loginError).toBe(false);
            });

            it('should not login due to a bad password', function() {
                runLogin('login_invalid_password', 'Bad password',
                         'real@example.com', 'bad'. false);

                expect(vm.loginForm).toEqual({
                    email: 'real@example.com'
                });
                expect(vm.loginError).toBe(true);
                expect(vm.loginErrorMsg).toEqual('Bad password');

                expect(currState).toEqual('home');
            });

            it('should not login since somebody is already logged in',
               function() {
                runLogin('login_dual', 'Already logged in',
                         'dual@example.com', 'whatever', false);

                expect(vm.loginForm).toEqual({});
                expect(vm.loginError).toBe(true);
                expect(vm.loginErrorMsg).toEqual('Already logged in');

                expect(currState).toEqual('error');
            });

            it('should create a client error due to missing data', function() {
                runLogin('', '', '', '', false);

                expect(vm.loginForm).toEqual({});
                expect(vm.loginError).toBe(true);
                expect(vm.loginErrorMsg)
                    .toBe('An unknown client-side error has occured');

                expect(currState).toEqual('error');
            });

            it('should also create a client error', function() {
                runLogin(undefined, undefined, '', '', false);

                expect(vm.loginForm).toEqual({});
                expect(vm.loginError).toBe(true);
                expect(vm.loginErrorMsg)
                    .toBe('An unknown client-side error has occured');

                expect(currState).toEqual('error');
            });

        });

        //----------------
        //  Register Tests
        //----------------

        describe('register', function() {

            it('should register a user', function() {
                runRegister('register_success', 'Register successful',
                            'test@example.com', 'test', 'test', true);

                expect(vm.registerForm).toEqual({});
                expect(currState).toEqual('dashboard');
            });

            it('should fail to register a user', function() {
                runRegister('register_already_exists', 'User already exists',
                            'test@example.com', 'test', 'test', false);

                expect(vm.registerForm).toEqual({});
                expect(currState).toEqual('home');
                expect(vm.registerError).toBe(true);
                expect(vm.registerErrorMsg).toBe('User already exists');

                // Close error and check if closed
                vm.closeRegisterError();
                expect(vm.registerError).toBe(false);
            });

            it('should fail to register with an error', function() {
                runRegister('register_bogus_code', 'something went wrong',
                            'test@example.com', 'test', 'test', false);

                expect(vm.registerForm).toEqual({});
                expect(currState).toEqual('error');
                expect(vm.registerError).toBe(true);
                expect(vm.registerErrorMsg).toBe('something went wrong');
            });

            it('should create a client error', function() {
                runRegister('', '', '', '', '', false);

                expect(vm.registerForm).toEqual({});
                expect(currState).toEqual('error');
                expect(vm.registerError).toBe(true);
                expect(vm.registerErrorMsg).toBe(
                    'An unknown client-side error has occured');
            });

            it('should also create a client error', function() {
                runRegister('', '', '', '', '', false);

                expect(vm.registerForm).toEqual({});
                expect(currState).toEqual('error');
                expect(vm.registerError).toBe(true);
                expect(vm.registerErrorMsg).toBe(
                    'An unknown client-side error has occured');
            });

            it('should fail due to a password mismatch', function() {
                runRegister('something_else', 'something',
                            'test@example.com', 'test', 'other', false);

                expect(vm.registerForm).toEqual({
                    email: 'test@example.com'
                });
                expect(currState).toEqual('home');
                expect(vm.registerError).toBe(true);
                expect(vm.registerErrorMsg).toBe('Passwords don\'t match');
            });

        });

        //---------------
        //  Extendability
        //---------------

        describe('extendTo', function() {

            it('should extend the controller', function() {
                var vm2 = vm.extendTo({});

                expect(vm2.loginErrorMsg).toBe('');
                expect(vm2.login).not.toBe(undefined);
            });

        });
    });
})();


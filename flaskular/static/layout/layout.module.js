/**
 * Module definition for layout.
 *
 * @namespace flaskular.layout
 */
(function(){
    'use strict';

    angular
        .module('flaskular.layout', [
            'flaskular.layout.topnav'
        ]);
    angular
        .module('flaskular.layout.topnav', [
            'flaskular.auth'
        ]);
})();

(function() {
    'use strict';

    //=========================================================================
    //  Topnav Controller
    //=========================================================================

    describe('TopnavController', function() {
        var vm;
        var template;
        var $provide;
        var stateStub;
        var authServiceStub;
        var topNavServiceStub;
        var q;
        var testScope;
        var currState;

        beforeEach(module('flaskular.layout.topnav'));

        beforeEach(module(function($urlRouterProvider) {
            $urlRouterProvider.deferIntercept();
        }));

        beforeEach(inject(function($rootScope, $compile, $controller, $q,
                                   $httpBackend) {

            q = $q;
            testScope = $rootScope.$new();
            $httpBackend
                .whenGET('core/layout/topnav.directive.html').respond();

            currState = 'something';
            stateStub = {
                go: sinon.spy(function(arg) {
                    currState = arg;
                }),
            };

            authServiceStub = {
                register: sinon.stub(),
                isLoggedIn: sinon.stub(),
                getCurrentUser: sinon.stub(),
                logout: sinon.stub(),
                hasRole: sinon.stub()
            };

            topNavServiceStub = {
                onTopnavToggle: sinon.stub()
            };

            var element = angular.element('<topnav></topnav>');
            template = $compile(element)(testScope);

            testScope.$digest();

            vm = $controller('TopnavController', {
                '$scope': testScope,
                '$state': stateStub,
                'authService': authServiceStub,
                'topnavService': topNavServiceStub
            });

        }));

        ///////////////////////////////////////////////////////////////////////
        //  Tests
        ///////////////////////////////////////////////////////////////////////

        //------------
        //  initialize
        //------------

        describe('initialization', function() {

            it('should have registered a listener with topnavService',
                    function() {
                expect(topNavServiceStub.onTopnavToggle.calledOnce).toBe(true);
            });

            it('should have set the login status', function() {
                expect(authServiceStub.isLoggedIn.calledOnce).toBe(true);
                expect(authServiceStub.getCurrentUser.calledOnce).toBe(true);
                expect(authServiceStub.hasRole.calledOnce).toBe(true);
            });

        });

        //----------------
        //  Set visibility
        //----------------

        describe('setVisibility', function() {

            it('should set show to false', function() {
                vm.setVisibility(false);
                expect(vm.show).toBe(false);
            });

            it('should set show to true', function() {
                vm.setVisibility(true);
                expect(vm.show).toBe(true);
            });

        });

        //--------
        //  logout
        //--------

        describe('logout', function() {

            it('should log the user out', function() {
                var expected = {
                    code: 'some_code'
                };
                var defer = q.defer();
                defer.resolve(expected);

                authServiceStub.logout.returns(defer.promise);

                vm.logout();
                testScope.$apply();

                expect(currState).toEqual('home');
            });

            it('should log the user out even with error', function() {
                var expected = {
                    code: 'some_code'
                };
                var defer = q.defer();
                defer.reject(expected);

                authServiceStub.logout.returns(defer.promise);

                vm.logout();
                testScope.$apply();

                expect(currState).toEqual('home');
            });
        });

        //---------------
        //  Extendability
        //---------------

        describe('extendTo', function() {

            it('should extend the controller', function() {
                var vm2 = vm.extendTo({});

                expect(vm2.show).toBe(true);
                expect(vm2.logout).not.toBe(undefined);
            });

        });

    });
})();

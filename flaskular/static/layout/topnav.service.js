/**
 * Service for topnav. Can turn topnav on or off with this.
 *
 * @namespace flaskular.layout.topnav
 */
(function() {
    'use strict';

    angular
        .module('flaskular.layout.topnav')
        .service('topnavService', topnavService);

    topnavService.$inject = [];

    /**
     * topnav service definition.
     */
    function topnavService() {
        var isOn = true;
        var topnavToggleListeners = [];

        var service = {
            onTopnavToggle : onTopnavToggle,
            toggleTopnav : toggleTopnav,
        };

        return service;

        ////////////////////////////////////////////
        //  Listeners
        ////////////////////////////////////////////

        /**
        * Registers a listener that is called whenever the topnav is toggled.
        *
        * @param {fcn (isOn)} Function that takes one parameter, `isOn` which is
        * true if the topnav was toggled on, false if it was toggled off.
        */
        function onTopnavToggle(callback) {
            topnavToggleListeners.push(callback);
        }

        ////////////////////////////////////////////
        //  Triggers
        ////////////////////////////////////////////

        /**
        * Triggers a toggle in the topnav visibility.
        *
        * @param {bool} setOn True to turn the topnav on, False to turn it off,
        * undefined to set it to the opposite of what it is now.
        */
        function toggleTopnav(setOn) {
            if (setOn === undefined) {
                setOn = !isOn;
            }
            isOn = setOn;

            angular.forEach(topnavToggleListeners, function (callback, ix) {
                callback(isOn);
            });
        }
    }
})();

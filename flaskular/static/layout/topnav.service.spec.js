(function() {
    'use strict';

    describe('topnavService Tests', function() {

        var topnavService;

        beforeEach(module('flaskular.layout.topnav'));

        beforeEach(inject(function(_topnavService_) {
            topnavService = _topnavService_;
        }));

        ///////////////////////////////////////////////////////////////////////
        //  Tests
        ///////////////////////////////////////////////////////////////////////

        describe('topnav toggle listener and callback', function() {

            it('should turn topnav on', function() {
                var callback = sinon.stub();
                topnavService.onTopnavToggle(callback);

                topnavService.toggleTopnav(true);

                expect(callback.calledOnce).toBe(true);
                expect(callback.calledWithExactly(true)).toBe(true);
            });

            it('should turn topnav off', function() {
                var callback = sinon.stub();
                topnavService.onTopnavToggle(callback);

                topnavService.toggleTopnav(false);

                expect(callback.calledOnce).toBe(true);
                expect(callback.calledWithExactly(false)).toBe(true);
            });

            it('should toggle off from init', function() {
                var callback = sinon.stub();
                topnavService.onTopnavToggle(callback);

                topnavService.toggleTopnav();

                expect(callback.calledOnce).toBe(true);
                expect(callback.calledWithExactly(false)).toBe(true);
            });

            it('should toggle off then on', function() {
                var callback = sinon.stub();
                topnavService.onTopnavToggle(callback);

                topnavService.toggleTopnav();

                expect(callback.calledOnce).toBe(true);
                expect(callback.calledWithExactly(false)).toBe(true);

                topnavService.toggleTopnav();

                expect(callback.calledTwice).toBe(true);
                expect(callback.calledWithExactly(true)).toBe(true);
            });
        });
    });
})();

(function() {

    describe('ProfileController', function() {

        var vm;
        var q;
        var stateStub;
        var authServiceStub;
        var testScope;
        var currState;
        var controller;

        beforeEach(module('flaskular.profile'));

        beforeEach(module(function($urlRouterProvider) {
            $urlRouterProvider.deferIntercept();
        }));

        beforeEach(inject(function($rootScope, $controller, $q) {
            q = $q;
            testScope = $rootScope.$new();
            controller = $controller;

            // Setup state
            currState = '/profile';
            stateStub = {
                go: sinon.spy(function(arg) {
                    currState = arg;
                })
            };

            // Setup authService
            authServiceStub = {
                getUserDict: sinon.stub(),
                changePassword: sinon.stub(),
                updateProfile: sinon.stub()
            };

            var deferred = q.defer();
            deferred.resolve({
                success: true,
                code: 'profile_success',
                userDict: {
                    id: 42,
                    email: 'example',
                    firstName: 'test',
                    lastName: 'user',
                    roles: ['subscriber']
                }
            });
            authServiceStub.getUserDict
                .returns(deferred.promise);

            // Build the controller
            vm = controller('ProfileController', {
                '$scope': testScope,
                '$state': stateStub,
                'authService': authServiceStub
            });

            testScope.$apply();

        }));

        ///////////////////////////////////////////////////////////////////////
        //  Helpers
        ///////////////////////////////////////////////////////////////////////

        /**
         * Runs a backend connection.
         *
         * @param {str} code The code returned by the authService.
         * @param {str} msg The message returned by the authService.
         * @param {fcn} str The backend function to run (one of
         * 'changePassword' or 'updateProfile')
         * @param {bool} success True if the authService resolves, false if it
         * rejects.
         */
        function runBackend(code, msg, fcn, success) {
            // Function backend
            var expected = {
                code: code,
                msg: msg,
                success: success
            };
            var defer = q.defer();

            if (success) {
                defer.resolve(expected);
            }
            else {
                defer.reject(expected);
            }

            authServiceStub[fcn]
                .returns(defer.promise);

            // Run the function
            vm[fcn]();
            testScope.$apply();

            // Check loading
            expect(vm.passwordLoading).not.toBe(true);
            expect(vm.profileLoading).not.toBe(true);
        }

        ///////////////////////////////////////////////////////////////////////
        //  Tests
        ///////////////////////////////////////////////////////////////////////

        describe('initialize', function() {

            it ('should successfully initialize the user profile', function() {
                expect(vm.user.id).toEqual(42);
                expect(vm.user.email).toEqual('example');
                expect(vm.user.firstName).toEqual('test');
                expect(vm.user.lastName).toEqual('user');
                expect(vm.user.roles.length).toEqual(1);
                expect(vm.user.roles[0]).toEqual('subscriber');

                expect(currState).toEqual('/profile');
            });

            it ('should fail to initialize', function() {
                var defer = q.defer();
                defer.reject({code: 'unknown', success: 'false'});
                authServiceStub.getUserDict.returns(defer.promise);

                var vm2 = controller('ProfileController', {
                    '$scope': testScope,
                    '$state': stateStub,
                    'authService': authServiceStub
                });

                testScope.$apply();

                expect(currState).toEqual('error');
            });

        });

        describe('passEqual', function() {

            it ('should be true', function() {
                vm.passForm.password = 'test';
                vm.passForm.repeat = 'test';

                expect(vm.passEqual()).toBe(true);
            });

            it ('should be true even if both are undefined', function() {
                expect(vm.passEqual()).toBe(true);
            });

            it ('should be false', function() {
                vm.passForm.password = 'test';
                vm.passForm.repeat = 't2';

                expect(vm.passEqual()).toBe(false);
            });

            it ('should be false with one undefined', function() {
                vm.passForm.password = 'test';

                expect(vm.passEqual()).toBe(false);
            });

        });

        describe('closePasswordError', function() {

            it ('should set the password error to false', function() {
                vm.passwordError = true;
                vm.closePasswordError();

                expect(vm.passwordError).toBe(false);
            });

        });

        describe('changePassword', function() {

            it ('should successfully change the password', function() {
                runBackend('password_change_success', 'Success',
                           'changePassword', true);

                expect(currState).toEqual('/profile');
                expect(vm.passwordSuccess).toBe(true);
                expect(vm.passForm).toEqual({});
            });

            it ('should fail with incorrect old password', function() {
                runBackend('password_change_incorrect_old', 'Incorrect old',
                           'changePassword', false);

                expect(currState).toEqual('/profile');
                expect(vm.passwordError).toBe(true);
                expect(vm.passwordErrorMessage).toEqual('Incorrect old');
                expect(vm.passForm.oldPassword).toEqual('');
            });

            it ('should fail completely', function() {
                runBackend('unknown', 'Something went wrong', 'changePassword',
                           false);

                expect(currState).toEqual('error');
            });

        });

        describe('updateProfile', function() {

            it ('should successfully update the profile', function() {
                runBackend('profile_change_success', 'Success',
                           'updateProfile', true);

                expect(currState).toEqual('/profile');
                expect(vm.profileSuccess).toBe(true);

            });

            it ('should fail completely', function() {
                runBackend('unknown', 'Fail', 'updateProfile', false);

                expect(currState).toEqual('error');

            });

        });

    });

})();

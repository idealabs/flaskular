/**
 * Module definition for profile. The profile module allows the user to manage
 * profile information (name, etc.) and to change their password.
 *
 * @namespace flaskular.profile
 */
(function() {
    'use strict';

    angular
        .module('flaskular.profile', [
            'ui.router',
            'flaskular.router',
            'ui.bootstrap'
        ]);
})();

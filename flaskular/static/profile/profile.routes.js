/**
 * Routes for the profile module.
 *
 * @namespace flaskular.profile
 */
(function() {
    'use strict';

    angular
        .module('flaskular.profile')
        .run(runBlock);

    runBlock.$inject = ['routerHelper'];

    function runBlock(routerHelper) {
        routerHelper.configureStates(getStates());
    }

    /**
     * Gets the route states.
     */
    function getStates() {
        return [
            {
                state: 'profile',
                config: {
                    templateUrl: 'core/profile/profile.html',
                    url: '/profile',
                    controller: 'ProfileController',
                    controllerAs: 'vm',
                    loginRequired: true
                }
            }
        ];
    }

})();

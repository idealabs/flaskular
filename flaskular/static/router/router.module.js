/**
 * Module definition for router.
 *
 * @namespace flaskular.router
 */
(function() {
    'use strict';

    angular
        .module('flaskular.router', [
            'ui.router',
            'flaskular.layout.topnav'
        ]);
})();

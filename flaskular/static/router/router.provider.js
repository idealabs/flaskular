angular
    .module('flaskular.router')
    .provider('routerHelper', routerHelperProvider);

routerHelperProvider.$inject = [
    '$locationProvider', '$stateProvider', '$urlRouterProvider'
];

/* @ngInject */
function routerHelperProvider($locationProvider, $stateProvider,
                              $urlRouterProvider) {
    this.$get = RouterHelper;

    RouterHelper.$inject = ['$state', '$stateParams', '$rootScope',
                            'topnavService', 'authService'];
    /* @ngInject */
    function RouterHelper($state, $stateParams, $rootScope, topnavService,
                          authService) {
        var hasOtherwise = false;

        var service = {
            configureStates: configureStates,
            initialize: initialize,
            getStates: getStates,
        };

        return service;

        ///////////////

        function initialize() {
            service.configureStates(getBaseStates(),
                                    getBaseAliases(),
                                    getBaseOtherwisePath());

            $rootScope.$on('$stateNotFound', function(event) {
                console.log(event);
                $state.go('error', {
                    'code': 'stateNotFound'
                });
            });

            $rootScope.$on('$stateChangeError', function(event) {
                console.log(event);
                $state.go('error', {
                    'code': 'stateNotFound'
                });
            });

            $rootScope.$on('$stateChangeStart',
                        function(event, toState, toParams, fromState,
                                    fromParams) {
                var debug = false;
                if (debug) {
                    console.log('stateChangeStart:');
                    console.log(event);
                    console.log(toState);
                    console.log(toParams);
                    console.log(fromState);
                    console.log(fromParams);
                }

                if (toState.name === 'home') {
                    topnavService.toggleTopnav(false);
                }
                else {
                    topnavService.toggleTopnav(true);
                }
            });

            $rootScope.$on('$stateChangeSuccess',
                        function(event, toState, toParams) {

                if (toState.name === 'home' && authService.isLoggedIn()) {
                    $state.go('dashboard', toParams);
                }
                else if (toState.loginRequired && !authService.isLoggedIn()) {
                    $state.go('home', toParams);
                }
                else if (!authService.hasAllRoles(toState.rolesRequired)) {
                    $state.go('error', {
                        code: 'not_authorized',
                        msg: 'You are not authorized for this view'
                    });
                }
                else if (!authService.hasOneRole(toState.rolesAccepted)) {
                    $state.go('error', {
                        code: 'not_authorized',
                        msg: 'You are not authorized for this view'
                    });
                }

            });

        }

        function configureStates(states, aliases, otherwisePath) {
            states.forEach(function(state) {
                $stateProvider.state(state.state, state.config);
            });
            if (aliases) {
                for (var i = 0; i < aliases.length; i++) {
                    var aliasTuple = aliases[i];
                    var alias = aliasTuple[0];
                    var existing = aliasTuple[1];

                    $urlRouterProvider.when(alias, existing);
                }
            }
            if (otherwisePath && !hasOtherwise) {
                hasOtherwise = true;
                $urlRouterProvider.otherwise(otherwisePath);
            }
        }

        function getStates() { return $state.get(); }

        /**
         * The default state routes.
         */
        function getBaseStates() {
            return [
                {
                    state: '404',
                    config: {
                        templateUrl: 'core/router/404.html',
                        url: '/404'
                    }
                },
                {
                    state: 'error',
                    config: {
                        url: '/error',
                        templateUrl: 'core/router/error.html',
                        params: {code: null, msg: null},
                        controller: function($state, $stateParams) {
                            var vm = this;

                            vm.code = $stateParams.code;
                            vm.msg = $stateParams.msg;
                        },
                        controllerAs: 'vm'
                    }
                }
            ];
        }

        /**
        * The aliases for existing states.
        */
        function getBaseAliases() {
            return [
            ];
        }

        /**
        * The view if route is missing.
        */
        function getBaseOtherwisePath() {
            return '/404';
        }
    }
}

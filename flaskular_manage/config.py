import os


class Config(object):
    DEBUG = False
    TESTING = False
    CSRF_ENABLED = True
    SECRET_KEY = 'changeme'
    SQLALCHEMY_DATABASE_URI = os.environ['DATABASE_URL']
    SQLALCHEMY_TRACK_MODIFICATIONS = False

    BASE_DIR = os.path.abspath(os.path.dirname(__file__))

    # Recommended: 2 per available processor core
    THREADS_PER_PAGE = 8

    CSRF_ENABLED = False
    CSRF_SESSION_KEY = 'changeme'
    WTF_CSRF_ENABLED = False
    WTRF_CSRF_ENABLED = False

    # Authentication
    SECURITY_PASSWORD_HASH = 'bcrypt'
    SECURITY_PASSWORD_SALT = SECRET_KEY
    SECURITY_REGISTERABLE = True
    SECURITY_RECOVERABLE = True
    SECURITY_TRACKABLE = True
    SECURITY_TOKEN_AUTHENTICATION_KEY = 'token'
    SECURITY_TOKEN_MAX_AGE = 60*60*24  # seconds before expiration

    # PROCESSORS
    TICK_CORES_AVAILABLE = 2  # cores available to ticker, -1 for all.


class ProductionConfig(Config):
    DEBUG = False


class ProductionConfigTesting(ProductionConfig):
    SQL_DATABASE_URI = os.environ['DATABASE_URL'] + '_test'
    TESTING = True

    SECURITY_TOKEN_MAX_AGE = 1


class StagingConfig(Config):
    DEVELOPMENT = True
    DEBUG = True


class StagingConfigTesting(StagingConfig):
    SQL_DATABASE_URI = os.environ['DATABASE_URL'] + '_test'
    TESTING = True

    SECURITY_TOKEN_MAX_AGE = 1


class DevelopmentConfig(Config):
    TESTING = True
    DEVELOPMENT = True
    DEBUG = True


class DevelopmentConfigTesting(DevelopmentConfig):
    SQLALCHEMY_DATABASE_URI = os.environ['DATABASE_URL'] + '_test'
    TESTING = True

    SECURITY_TOKEN_MAX_AGE = 1  # Seconds before expiration. Short for tests.

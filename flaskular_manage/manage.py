"""
Manages the database migrations using Alembic and Flask-Migrate. See README.md
for usage instructions.
"""

from flask_script import Manager
from flask_migrate import Migrate, MigrateCommand


def build_manager(app, db):
    Migrate(app, db)
    manager = Manager(app)

    manager.add_command('db', MigrateCommand)

    return manager

from setuptools import Command
from setuptools.command.test import test as TestCommand
from setuptools.command.install import install as InstallCommand
from setuptools.command.develop import develop as DevelopCommand
import sys
import os
from subprocess import call


def _yarn_install():
    print('\n\n>> Installing Client Dependencies (Yarn) >>\n\n')
    os.system('yarn')
    print('\n\n<< Client Dependencies Installed <<\n\n')


class Install(InstallCommand):

    def run(self):
        InstallCommand.run(self)
        _yarn_install()


class Develop(DevelopCommand):

    def run(self):
        DevelopCommand.run(self)
        _yarn_install()


class PyTest(TestCommand):

    user_options = [('test-only=', 't',
                     'Tests only the specified module in app'),
                    ('front-only', 'f',
                     'Run only the frontend (Angular) tests'),
                    ('back-only', 'b',
                     'Run only the backend (Python) tests')]
    project_dir = 'flaskular'

    def initialize_options(self):
        self.test_only = None
        self.front_only = None
        self.back_only = None
        TestCommand.initialize_options(self)

    def finalize_options(self):
        os.system('printf "\033c"')
        TestCommand.finalize_options(self)

        self.test_args = ['--cov', '--cov-report', 'term-missing']

        if self.test_only is not None:
            test_dir = '{}/{}'.format(self.project_dir, self.test_only)
            self.test_args = [test_dir] + self.test_args
            # TODO configure karma to run single modules
        else:
            self.test_args = [self.project_dir] + self.test_args

        self.test_suite = True

    def run_tests(self):
        import pytest

        if self.front_only and self.back_only:
            raise Exception('Cannot run using both front only and back only')

        if not self.front_only:
            print('\n\n-- Running Backend (Python) Tests --\n\n')
            errno = pytest.main(self.test_args)
            if errno != 0:
                sys.exit(errno)

        if not self.back_only:
            print('\n\n-- Running Frontend (Angular) Tests --\n\n')
            os.system('./node_modules/karma/bin/karma start')

        sys.exit(0)


class PyDoc(Command):
    description = 'run documentation'
    user_options = []

    def initialize_options(self):
        pass

    def finalize_options(self):
        pass

    def run(self):
        cwd = os.getcwd()
        os.chdir('public/docs/')
        call(['make', 'html'])
        os.chdir(cwd)

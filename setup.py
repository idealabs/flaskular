from setuptools import setup

from flaskular_manage import (FlaskularInstall, FlaskularDevelop,
                              FlaskularPyTest, FlaskularPyDoc)


class Install(FlaskularInstall):
    pass


class Develop(FlaskularDevelop):
    pass


class PyTest(FlaskularPyTest):
    pass


class PyDoc(FlaskularPyDoc):
    pass


setup(name='flaskular',
      version='0.1.0',
      packages=['flaskular', 'flaskular_manage'],
      include_package_data=True,
      install_requires=[
          'sqlalchemy',
          'flask',
          'flask-migrate',
          'flask-script',
          'flask-security',
          'flask-sqlalchemy',
          'flask-testing',
          'psycopg2',
          'alembic',
          'Flask-Migrate',
          'bcrypt',
          'nltk',
          'requests',
          'uwsgi',
          'colored'
      ],
      tests_require=[
          'pytest',
          'pytest-cov',
      ],
      cmdclass={
          'install': Install,
          'develop': Develop,
          'test': PyTest,
          'document': PyDoc
      })
